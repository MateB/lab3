//Mate Barabas 1834578
package LinearAlgebra;

public class Vectors {

	public static void main(String[] args) 
	{
		Vector3d vec = new Vector3d(1,1,2);
		Vector3d v = new Vector3d(2,3,4);
		
		System.out.println("Dot product output: " + vec.dotProduct(v));
		System.out.println("magnitude output: " + vec.magnitude());
		System.out.println("add method output: " + vec.add(v).toString());
	}

}
