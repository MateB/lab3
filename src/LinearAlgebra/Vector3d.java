//Mate Barabas 1834578
package LinearAlgebra;

public class Vector3d
{
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX()
	{
		return this.x;
	}
	
	public double getY()
	{
		return this.y;
	}
	
	public double getZ()
	{
		return this.z;
	}
	
	public double magnitude()
	{
		double magnitude;
		magnitude = Math.sqrt((Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2)));
		return magnitude;
	}
	
	public double dotProduct(Vector3d v)
	{
		double dotProduct;
		dotProduct = (this.x * v.getX()) + (this.y * v.getY()) + (this.z * v.getZ());
		return dotProduct;
	}
	
	public Vector3d add(Vector3d v)
	{
		double nX = this.x + v.getX();
		double nY = this.y + v.getY();
		double nZ = this.z + v.getZ();
		Vector3d newV = new Vector3d(nX, nY, nZ);
		return newV;
	}
	
	public String toString()
	{
		return "x: " + this.x + " y: " + this.y + " z: " + this.z;
	}
}
