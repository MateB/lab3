//Mate Barabas 1834578
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	public void getTest() 
	{
		Vector3d test = new Vector3d(2,3,4);
		assertEquals(2, test.getX());
		assertEquals(3, test.getY());
		assertEquals(4, test.getZ());
	}
	
	@Test
	public void magnitudeTest()
	{
		Vector3d t = new Vector3d(2,4,4);
		assertEquals(6, t.magnitude());
	}
	
	@Test
	public void dotProductTest()
	{
		Vector3d vec = new Vector3d(1,1,2);
		Vector3d v = new Vector3d(2,3,4);
		
		assertEquals(13, vec.dotProduct(v));
	}
	
	@Test
	public void addTest()
	{
		Vector3d vec = new Vector3d(1,1,2);
		Vector3d v = new Vector3d(2,3,4);
		
		assertEquals(3, vec.add(v).getX());
		assertEquals(4, vec.add(v).getY());
		assertEquals(6, vec.add(v).getZ());
	}
}
